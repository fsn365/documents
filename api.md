# API documents

- version: 0.0.1
- host：api.fsn365.com(For online version)
- accepts: application/json
- responds with: application/json

All the API endpoints response data in a format of:

```ts
interface response {
  data?:any;
  ok:boolean; // succesful response, ok is true, otherwise is false,
  msg?:string; // error msg
}
```
For a successful request resposne, the status code is always 200,
for a failed request response, the status code varies.

## Transactions
- transaction types:
```ts
export const TRANSACTION_TYPES = {
  unknown: -1,
  GenNotationFunc: 0,
  GenAssetFunc: 1,
  SendAssetFunc: 2,
  TimeLockFunc: 3,
  OldAssetValueChangeFunc: 4,
  MakeSwapFunc: 5,
  RecallSwapFunc: 6,
  TakeSwapFunc: 7,
  EmptyFunc: 8,
  MakeSwapFuncExt: 9,
  TakeSwapFuncExt: 10,
  AssetValueChangeFunc: 11,
  MakeMultiSwapFunc: 12,
  RecallMultiSwapFunc: 13,
  TakeMultiSwapFunc: 14,
  ReportIllegalFunc: 15,
  Origin: 16,
  ERC20: 17,
  TokenPurchase: 18,
  EthPurchase: 19,
  AddLiquidity: 20,
  RemoveLiquidity: 21,
  Approval: 22,
  CreateContract: 23,
};
```
- **Get** &nbsp; &nbsp; /txs
  - Usage: Get processed transactions.
  - Parameters:
    ```ts
    interface query {
      page?: number;
      size?: number;
      address?: string; // transaction made or received by this address
      asset?: string; // asset related with this transaction
      type?: number; // transaction type
    }
    ```
  - Response
    ```ts
    interface transaction {
      hash: string;
      status: number;
      sender: string;
      receiver: string;
      type: number;
      data?: object;
      assets?: string[];
      block: number;
      timestamp: number;
      fee: string;
    }
    interface response {
      ok: boolean;
      data: {
        data: transaction[];
        total: number;
      }
    } 
    ```
- **Get** &nbsp; &nbsp; /tx/: hash
  - Usage: Get transaction detail by transaction hash.
  - Parameter: hash
  - Response
    ```ts
    interface transaction {
      hash: string;
      status: number;
      sender: string;
      receiver: string;
      type: number;
      data?: object;
      assets?: string[];
      block: number;
      timestamp: number;
      fee: string;
    }
    interface response {
      ok: boolean;
      data: transaction;
    } 
    ```

## Blocks

- **Get** &nbsp;&nbsp; /block/:height
  - Usage: Get block information by block height;
  - Parameter: height
  - Response:
    ```ts
    interface block {
      height: number;
      hash: string;
      miner: string;
      size: number;
      gasLimit: number;
      gasUsed: number;
      timestamp: number; 
      txs: number;
      reward: string;
    }
    interface response {
      ok: boolean;
      data: block;
    }
    ```
- **Get** &nbsp; &nbsp; /blocks
  - Usage: Get blocks
  - Parameters:
    ```ts
    interface query {
      page?: number;
      size?: number;
      order?: string; // desc or asc, order by block heiht
    }
    ```
  - Response
    ```ts
    interface block {
      txs: number;
      miner: string;
      height: number;
      timestamp: number;
    }

    interface response {
      ok: true;
      data: {
        data: block[];
        total: number;
      }
    }
    ```

## Assets

- **Get** &nbsp; &nbsp; /assets
  - Usage: Get fusion assets
  - Parameters:
    ```ts
    interface query {
      page?:number;
      size?:number;
      keyword?:string; // asset's name or asset's symbol
    }
    ```
    - Response
    ```ts
    interface asset {
      hash: string;
      name: string;
      symbol: string;
      issue_tx: string;
      issue_time: number;
      description?: object;
    }
    interface response {
      ok: boolean;
      data: {
        data: asset [],
        total: number;
      }
    }
    ```
- **Get** &nbsp; &nbsp; asset/: hash
  - Usage: Get asset's information.
  - Parameter: asset hash
  - Response
    ```ts
    interface asset {
      hash: string;
      name: string;
      symbol: string;
      issue_tx: string;
      issue_time: number;
      descrption?: object;
      precision: string;
      qty: string;
      canchange: boolean;
      verfied: boolean;
      description?: object;
      holders: number;
    }
    interface response {
      ok: boolean;
      data: asset;
    }
    ```
- **Get** &nbsp; &nbsp; asset/: hash/txs
  - Usage: Get transactions related with this asset
  - Notice: We only provide 10k transactions most for this endpoints.
  - Parameters:
      ```ts
        interface query {
          page?: number;
          size?: number;
          type?: number; // transaction type
        }
      ```
  - Response
    ```ts
      interface transaction {
        hash: string;
        status: number;
        sender: string;
        receiver: string;
        type: number;
        data: object;
        assets: string[];
        block: number;
        timestamp: number;
        fee: string;
      }
      interface response {
        ok: boolean;
        data: {
          data: transaction[];
          total: number;
        }
      }  
    ```
    
 - **GET** &nbsp; &nbsp; /asset/: hash/holders
    - Usage: get asset's rich list
    - Notice: We only provide top 1k rich list at most, and the order is by qty_in by default.
    - Parameters
      ```ts
      interface query {
        page?: number;
        size?: number;
      }
      ```
    - Response
      ```ts
      interface asset_holder {
        address:string;
        asset: string;
        symbol: string;
        qty_in: numer;
        qty: number;
        qty_own: number;
      }
      interface response {
        ok: boolean;
        data: {
          data: asset_holder [];
          total: number; // 1000 at most
        }
      }
      ```

